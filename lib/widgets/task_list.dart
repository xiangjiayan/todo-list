import 'package:flutter/material.dart';
import 'package:todo/widgets/task_tile.dart';
import 'package:todo/models/task_data.dart';
import 'package:provider/provider.dart';

class TaskList extends StatefulWidget {
  @override
  _TaskListState createState() {
    return _TaskListState();
  }
}

class _TaskListState extends State<TaskList> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) {
        return TaskTile(
          taskTitle: Provider.of<TaskData>(context, listen: false)
              .getTaskDescription(index),
          isChecked: Provider.of<TaskData>(context, listen: false)
              .getTaskIsDone(index),
          checkboxCallback: (checkboxState) {
            setState(() {
              Provider.of<TaskData>(context, listen: false).updateTask(index);
            });
          },
          deleteCallback: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('Confirm Delete'),
                  content: Text('Are you sure you want to delete this task?'),
                  actions: <Widget>[
                    TextButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    TextButton(
                      child: Text('Delete'),
                      onPressed: () {
                        setState(() {
                          Provider.of<TaskData>(context, listen: false)
                              .deleteTask(index);
                        });
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
          },
        );
      },
      itemCount: Provider.of<TaskData>(context).taskCount,
    );
  }
}

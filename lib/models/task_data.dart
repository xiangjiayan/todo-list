import 'dart:ffi';

import 'package:flutter/foundation.dart';
import 'package:todo/models/task.dart';

class TaskData extends ChangeNotifier {
  List<Task> _tasks = [
    Task(taskDescription: 'Pick up Home Depot order'),
    Task(taskDescription: 'Buy groceries'),
    Task(taskDescription: 'Pick up dry cleaning'),
    Task(taskDescription: 'Confirm travel plans'),
  ];

  int get taskCount {
    return _tasks.length;
  }

  void addTask(String newTaskTitle) {
    final task = Task(taskDescription: newTaskTitle);
    _tasks.add(task);
    notifyListeners();
  }

  void updateTask(int index) {
    Task task = _tasks[index];
    task.toggleDone();
    notifyListeners();
  }

  void deleteTask(int index) {
    _tasks.remove(_tasks[index]);
    notifyListeners();
  }

  String getTaskDescription(int index) {
    return _tasks[index].taskDescription;
  }

  bool getTaskIsDone(int index) {
    return _tasks[index].isDone;
  }
}

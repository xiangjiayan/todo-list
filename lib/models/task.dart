class Task {
  Task({required this.taskDescription, this.isDone = false});

  final String taskDescription;
  bool isDone = false;

  void toggleDone() {
    isDone = !isDone;
  }
}
